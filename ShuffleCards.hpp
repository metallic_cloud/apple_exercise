#pragma once


namespace AppleProblem
{
   size_t RoundsToReturnDeck(int deckSize);
}
