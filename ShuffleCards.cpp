
#include "CircularBuffer.hpp"
#include "ShuffleCards.hpp"

namespace
{
void performRound(CircularBuffer& deckInHand, CircularBuffer& deckOnTable)
{
   while (!deckInHand.isEmpty())
   {
      deckOnTable.pushFront(deckInHand.front());
      deckInHand.popFront();

      if (!deckInHand.isEmpty())
      {
         int topCard = deckInHand.front();
         deckInHand.popFront();
         deckInHand.pushBack(topCard);
      }
   }

   deckInHand = deckOnTable; // Would use std::move if allowed
   deckOnTable.clear();
}
}

size_t AppleProblem::RoundsToReturnDeck(int deckSize)
{
   CircularBuffer startingDeck((size_t)deckSize);

   for (int i = 0; i < deckSize; ++i)
   {
      // I could make a card class, but don't think it's necessary for this.
      startingDeck.pushBack(i);
   }

   CircularBuffer deckInHand = startingDeck;
   CircularBuffer deckOnTable(deckSize);

   size_t rounds = 0;

   do
   {
      performRound(deckInHand, deckOnTable);
      rounds++;
   } while (deckInHand != startingDeck);

   return rounds;
}