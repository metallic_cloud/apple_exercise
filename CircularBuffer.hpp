#pragma once


class CircularBuffer
{
public:

   explicit CircularBuffer(size_t bufferSize);
   ~CircularBuffer();

   CircularBuffer(const CircularBuffer& other);
   CircularBuffer& operator=(const CircularBuffer& other);

   bool isEmpty() const;
   bool isFull() const;

   /**
    * Adds an item to the front of the buffer.  Returns false if the buffer is full.
    */
   bool pushFront(int item);

   /**
    * Adds an item to the back of the buffer.  Returns false if the buffer is full.
    */
   bool pushBack(int item);

   /**
    * Remove the item at the front of the buffer.  No effect if empty.
    */
   void popFront();

   /**
    * Returns the value of the item at the front of the buffer.  Does not remove it.
    * If the buffer is empty, it will return -1;
    */
   int front() const;

   /**
    * Returns the value of the item at the back of the buffer.  Does not remove it.
    * If the buffer is empty, it will return -1;
    */
   int back() const;

   /**
    * The total number of items the buffer can hold
    */
   size_t capacity() const;

   /**
    * Returns the current number of items in the buffer
    */
   size_t size() const;

   /**
    * Returns how many more items can be added to the buffer
    */
   size_t freeSpace() const;

   void clear();

   bool operator==(const CircularBuffer& other);
   bool operator!=(const CircularBuffer& other);

private:
   static int peekAndIncrement(const CircularBuffer& buffer, size_t& position);

   int* m_buffer;
   size_t m_bufferSize;

   size_t m_front = 0;
   size_t m_back = 0;
   bool m_isEmpty = true;
};

