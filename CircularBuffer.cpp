
#include "CircularBuffer.hpp"

#include <cassert>
#include <cstring>

namespace
{
   size_t getPrev(size_t current, size_t bufferSize)
   {
      return current == 0 ? bufferSize - 1 : current - 1;
   }

   size_t getNext(size_t current, size_t bufferSize)
   {
      return current == bufferSize - 1 ? 0 : current + 1;
   }
}

CircularBuffer::CircularBuffer(size_t bufferSize) :
   m_buffer(new int[bufferSize]),
   m_bufferSize(bufferSize)
{
   assert(bufferSize > 0);
}

CircularBuffer::~CircularBuffer()
{
   delete[] m_buffer;
}

CircularBuffer::CircularBuffer(const CircularBuffer& other) :
   m_buffer(new int[other.m_bufferSize]),
   m_bufferSize(other.m_bufferSize),
   m_front(other.size()),
   m_back(0),
   m_isEmpty(other.isEmpty())
{
   size_t otherPosition = other.m_back;

   for (size_t i = 0; i < other.size(); ++i)
   {
      m_buffer[i] = peekAndIncrement(other, otherPosition);
   }
}

CircularBuffer& CircularBuffer::operator=(const CircularBuffer& other)
{
   if (m_bufferSize != other.m_bufferSize)
   {
      delete[] m_buffer;
      m_bufferSize = other.m_bufferSize;
      m_buffer = new int[m_bufferSize];
   }

   m_front = other.size();
   m_back = 0;
   m_isEmpty = other.isEmpty();
   
   size_t otherPosition = other.m_back;

   for (size_t i = 0; i < other.size(); ++i)
   {
      m_buffer[i] = peekAndIncrement(other, otherPosition);
   }

   return *this;
}

bool CircularBuffer::isEmpty() const
{
   return m_isEmpty;
}

bool CircularBuffer::isFull() const
{
   return !m_isEmpty && m_front == m_back;
}

bool CircularBuffer::pushFront(int item)
{
   if (isFull())
   {
      return false;
   }

   // Front will always point to the first empty space
   m_buffer[m_front] = item;
   m_front = getNext(m_front, m_bufferSize);
   m_isEmpty = false;

   return true;
}

bool CircularBuffer::pushBack(int item)
{
   if (isFull())
   {
      return false;
   }

   // Back will always point to the first element, not an empty space
   m_back = getPrev(m_back, m_bufferSize);
   m_buffer[m_back] = item;
   m_isEmpty = false;

   return true;
}

void CircularBuffer::popFront()
{
   if (!isEmpty())
   {
      m_front = getPrev(m_front, m_bufferSize);
      m_isEmpty = m_back == m_front;
   }
}

int CircularBuffer::front() const
{
   if (isEmpty())
   {
      // Would throw an exception normally
      return -1;
   }

   return m_buffer[getPrev(m_front, m_bufferSize)];
}

int CircularBuffer::back() const
{
   if (isEmpty())
   {
      // Would throw an exception normally
      return -1;
   }

   return m_buffer[m_back];
}

size_t CircularBuffer::capacity() const
{
   return m_bufferSize;
}

size_t CircularBuffer::size() const
{
   if (m_isEmpty)
   {
      return 0;
   }

   if (m_back < m_front)
   {
      return m_front - m_back;
   }

   return m_bufferSize - m_back + m_front;
}

size_t CircularBuffer::freeSpace() const
{
   return capacity() - size();
}

void CircularBuffer::clear()
{
   m_isEmpty = true;
   m_front = 0;
   m_back = 0;
}

bool CircularBuffer::operator==(const CircularBuffer& other)
{
   size_t thisSize = size();
   if (thisSize != other.size())
   {
      return false;
   }

   size_t thisPosition = m_back;
   size_t otherPosition = other.m_back;

   for (size_t i = 0; i < thisSize; ++i)
   {
      if (peekAndIncrement(*this, thisPosition) !=
         peekAndIncrement(other, otherPosition))
      {
         return false;
      }
   }

   return true;
}

bool CircularBuffer::operator!=(const CircularBuffer& other)
{
   return !(*this == other);
}

int CircularBuffer::peekAndIncrement(const CircularBuffer& buffer, size_t& position)
{
   int val = buffer.m_buffer[position];
   position = getNext(position, buffer.m_bufferSize);
   return val;
}