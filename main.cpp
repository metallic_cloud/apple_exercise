#pragma once

#include "CircularBuffer.hpp"
#include "CircularBufferTests.hpp"
#include "ShuffleCards.hpp"

#include <cstdlib>
#include <iostream>

void runTests()
{
   std::cout << "Running Circular buffer tests...\n";
   bool result = CircularBufferTests::run();
   std::cout << "Test result: " << (result ? "OK" : "FAILED!") << "\n\n";

   std::cout << "Running Apple Problem tests...\n";

   auto testRun = [](size_t cardCount, size_t expected)
   {
      size_t rounds = AppleProblem::RoundsToReturnDeck(cardCount);
      if (rounds != expected)
      {
         std::cout << "Failed shuffle test.  Card count: " << cardCount << ", result: " << rounds << ", expected: " << expected << "\n";
         return false;
      }

      return true;
   };

   result =
      testRun(1, 1) &&
      testRun(2, 2) &&
      testRun(3, 3) &&
      testRun(5, 5) &&
      testRun(6, 6) &&
      testRun(7, 5) &&
      testRun(10, 6);

   std::cout << "Test result: " << (result ? "OK" : "FAILED!") << "\n\n";
}

int main(int argc, char* argv[])
{
   if (argc < 2)
   {
      std::cout << "Number of cards not passed to program\n";
      return 1;
   }

   int cardCount = atoi(argv[1]);

   if (cardCount == 0)
   {
      std::cout << "Invalid number of cards passed: " << cardCount << " - Expected an integer > 0.  Pass a value < 0 to run tests\n";
      return 1;
   }

   if (cardCount > 0)
   {
      size_t rounds = AppleProblem::RoundsToReturnDeck(cardCount);
      std::cout << "Result: It took " << rounds << " rounds to return the deck to its initial position\n";
   }
   else
   {
      runTests();
   }

   return 0;
}